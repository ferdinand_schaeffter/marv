# MARV - Mobile akustische RaumVermessung

Dieses Projekt dreht sich um die Erkennung von Räumen mithilfe von Lautsprecher und mindestens 4 Mikrofonen. Das Aufnahmeprogramm MARV-Jack.py funktioniert mit benötigt einen laufenden JACK Server.
Es sind Beispielaufnahmen in dem Ordner A1 (Klavierzimmer/kleiner möbilierter Raum) zu finden, in denen zudem eine Infodatei und das Ausgangssignal als numpy datei gespeichert sind.

1. Aufnahme
Die Aufnahme erfolgt mit MARV-JACK.py. 
    - Hierbei muss das Ausgangssignal definiert werden (Frequenzbereich und Dauer). 
    - Außerdem müssen 4 Mikrofone angeschlossen sein. 
    - Für die Auswertung ist eine Distanzmatrix der Mikrofone und der Lautsprecher erforderlich, daher ist es sinnvoll vor der Aufnahme diese zu messen festzuhalten. 
    - Standardt mäßig führt das Programm 10 Aufnahmen hintereinander durch, die separat gespeichert werden.

2. Auswertung
Die Auswertung erfolgt mit MARV_Analysis.py. 
    - Hierbei muss unter loadpath der Aufnahmeordner angegeben werden 
    - die gewünschten Aufnahmen in der funktion Corr_calc gibt die Variable usedchirp an welche Aufnahme verwendet werden soll. 
    - Sollten neue Aufnahmen durchgeführt werden, bitte die entsprechende Distanz Matrix eintragen 
    - ausreichend bekannte in MicPos festlegen um das Koordinaten System zu fixieren. 
    - Die Ausgabe erfolgt über in die Auswertung.scad Datei, welche mit Openscad geöffnet werden kann.   

Andere Dateien:

- calc.py beinhaltet wichtige Funktionen für die Auswertung.
- MARV_Sudoku enthält Klassen, die für die Bildquellen filterung und Raumberechnung verfwendet werden.
- Im Ordner NeueEchoerkennung ist Code für den neuen WIP Algoritmus zur Echoerkennung.
- klavierzimmer.scad beinhaltet den erwarteten Raum der Aufnahme A1