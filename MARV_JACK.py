# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'

# %%
from utils import set_axis_stuff
import wave
import struct
import numpy as np
from scipy.signal import chirp 
import os
import wave
import time
from scipy.io import wavfile
from scipy import signal
from scipy import constants
import numpy as np
import matplotlib
#from json import dump

from scipy import fftpack
import matplotlib.pyplot as plt
from matplotlib.ticker import ScalarFormatter
import matplotlib.ticker as mticker
import threading
import logging
import jack
import socket
import queue
import collections
from scipy.fft import fft, fftfreq
#from numba import jit





# %%

#figure naming
def getNextFilePath(output_folder,name):
    highest_num = 0
    for f in os.listdir(output_folder):
        if os.path.isfile(os.path.join(output_folder, f)):
            file_name = os.path.splitext(f)[0]
            file_name=file_name[-1:]
            try:
                file_num = int(file_name)
                if file_num > highest_num:
                    highest_num = file_num
            except ValueError:
                'The file name "%s" is not an integer. Skipping' % file_name

    output_file = os.path.join(output_folder, name+str(highest_num + 1))
    return output_file
# %%
# Funktion die den Chirp abspielt wird in einem Thread gestartet
def playrec(client, play, recording, chirplen, chunked):
    t = threading.currentThread()
    def process(frames):
        data =play.get_nowait()
        for port in client.outports:
            port.get_array()[:] = data
        for port in client.inports:
            recording.put_nowait(port.get_array())
    
    #start client
    n=0
    client.set_process_callback(process)
    client.activate()
    play_port = client.outports.register("out")
    rec=[]
    channels=0
    # connect and register ports
    for port in client.get_ports(is_audio=True,is_physical=True,is_output=True):
        rec.append(client.inports.register("in"+str(channels),is_terminal=True))
        client.connect(port,rec[channels])
        channels+=1
 
    for port in client.get_ports(is_audio=True,is_physical=True,is_input=True):
        play_port.connect(port)

    #for looping playback
    while getattr(t, "terminate", False):
        n+=1
        for i in range(chunked.shape[0]):
            play.put(chunked[i],timeout=10)
        print("gespielt:",n)
        if n>10:#stop after 10 chirps
            break
    
    client.deactivate()
    client.close()
        

def storage(datastorage, chirplen, recording ,channels,savepath, samplerate=44000,fstop=15000, blocksize=1024):
    t = threading.currentThread()
    maxamp=0.001 # muss ot angepasst werden
    triggerdelay = -50 # in Milliseconds
    delayblocks = int((triggerdelay * samplerate / (1000 * blocksize)))
    n=0
    chirpbuffer = [[]]*channels
    for c in range(channels):
        chirpbuffer[c]=collections.deque(maxlen = chirplen)
    def setmax(cur_max,cur_value):# 
        if cur_value > cur_max:
            print("new max:", cur_value)
            return cur_value
        else:
            return cur_max
    def fillbuffer():#fill local buffer with data from recording quque
        nonlocal chirpbuffer
        for c in range(channels):
            data_in = recording.get(timeout=10)
            for i in range(len(data_in)):
                chirpbuffer[c].append(data_in[i])
        return data_in

    while getattr(t, "terminate", False): #Running all the time
        current_in=fillbuffer() # könnte Probleme machen wenn buffer/blocksize zu klein         
        #check if last frequencie of chirp is playing
        cf,ct,current_stft = signal.stft(current_in,fs=samplerate)
        index_fstop=0
        for i in range(len(cf)):
            if np.abs(cf[i]-fstop) < np.abs(cf[index_fstop]-fstop):
                index_fstop=i
        fstop_ampltiude = np.sum(np.abs(current_stft[index_fstop]))
        maxamp=setmax(maxamp,fstop_ampltiude)
        trigger = fstop_ampltiude>maxamp*0.5
        
        if trigger:
            for i in range(delayblocks): #delay für zu frühes auslösen
                if getattr(t, "terminate", False):
                    fillbuffer()
                else:
                    break
            chirpdata = np.zeros((channels, chirplen))
            if len(chirpbuffer[0]) != chirplen:# ist der Buffer voll?
                print("no full chirp")
                continue
            else:
                for c in range(channels):
                    for j in range(chirplen):
                        chirpdata[c][j] = chirpbuffer[c].popleft()    

            #recording over
            datastorage.put(chirpdata) # Send data off to calculation
            n+=1
            print("recorded chirp ",n)
            if getattr(t, "save", True):
                np.save(savepath+"/"+str(n),chirpdata)



def storage_cut(chirplen, recording ,channels,chirpvalues):#attempt on more flexible recording process
    windowsize=chirplen*1.5
    channels=4
    t = threading.currentThread()
    chirpbuffer = [[0]]*channels 
    print(np.shape(chirpbuffer))
    def fillbuffer():
        nonlocal chirpbuffer
        for c in range(channels):
            data_in = recording.get(timeout=10)
            for i in range(len(data_in)):
                chirpbuffer[c].append(data_in[i])
        return data_in
    while getattr(t, "terminate", False):
        try:
            fillbuffer()
        except queue.Empty:
            print("Empty") 
        # for c in range(channels):
        #     data_in = recording.get(timeout=6)
        #     for i in range(len(data_in)):
        #         chirpbuffer[c].append(data_in[i])
        #wenn nix is mach nix
        print(np.shape(chirpbuffer[c]))
        print("peakfinding...")
        #peaks,_ = signal.find_peaks(cutcorr)
        print("sorting...")
        #order = np.argsort(cutcorr[peaks])
        #plt.plot(chirpbuffer[0])
        #plt.vlines(peaks,ymin=-1,ymax=1)
        print("order")
        #plt.show()
        t.terminate= False
        break
            

def chirpgenerator(play,blocksize,fstart = 1000, fstop=15000,chirp_dur=0.3,fs = 96000,channels=1):#generates chirp useable for playback
    #samples?
    samples=int(fs*chirp_dur)
    overhead=samples % blocksize 
    samples = samples-overhead
    chirp_dur= samples/fs
    #t u. buffer
    t=np.linspace(0,chirp_dur,samples)
    buffer=np.zeros(2*samples)
    #chirp
    chirp_values=chirp(t,fstart,chirp_dur,fstop,method="hyp") # 10000 only for laptop recordings bc bad microphones
    values=np.append(chirp_values,buffer)
    values=np.append(buffer,values)
    # schreiben in die queue
    chunked=values.reshape(int(len(values)/blocksize),blocksize)
    #for i in range(chunked.shape[0]):
    #    play.put_nowait(chunked[i])
    return (chirp_values, samples, chunked)
    



def main():
    #mode
    Save=True # save generated chirp and recordings
    #setup client
    client = jack.Client("ARA")
    #read parameters of client
    block_size=client.blocksize
    channels = len(client.get_ports(is_audio=True,is_physical=True,is_output=True))
    fs = client.samplerate
    print(fs)
    qsize=1000 #Size of queueus used in process
    # initialize queue
    play = queue.Queue(maxsize=qsize)  
    recording = queue.Queue(maxsize=qsize)# for audio outputs
    datastorage = queue.Queue()# for chirp analysis
    # Generate Chirp on the fly
    fstop=19000
    chirp_values, chirplen, chunked = chirpgenerator(play,block_size,channels=channels,fs=fs,fstart=1000,fstop=fstop,chirp_dur=0.5)
    print("chirplen: ", chirplen)
    print("channels: ", channels)
    print("blocksize: ", block_size)
    # Set Recording Path
    savepath = f"./{int(time.time()-1602612611.730985)}"
    # save chirp 
    if Save == True:
        os.mkdir(savepath)
        np.save(savepath+"/chirp",chirp_values)

    

    # initialize Threads
    playThread = threading.Thread(target=playrec, args=(client, play, recording, chirplen, chunked)) # might cause problems if channels too  high
    storeThread = threading.Thread(target=storage, args=(datastorage, chirplen, recording,channels,savepath,fs,fstop))
    #storeThread = threading.Thread(target=storage_cut, args=(chirplen,recording,channels,chirp_values))
    #set up
    playThread.terminate = True
    storeThread.terminate = True
    storeThread.save = Save #True to save data in .npy file

    # start Threads
    playThread.start() 
    storeThread.start()
    
    #wait for input the stop
    print('Press any key to stop')
    # input()
    # Terminate Threads
    # playThread.terminate = False
    # storeThread.terminate = False
    playThread.join()
    storeThread.join()
    
    #plt.show()
    
    
if __name__ == "__main__":
    time.sleep(1)
    main()

# %%
