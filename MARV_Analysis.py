
from utils import set_axis_stuff
import wave
import struct
import numpy as np
import pandas as pd
from scipy.signal import chirp 
import os
import wave
import time
from scipy.io import wavfile
from scipy import signal
from scipy import constants
import numpy as np
import matplotlib
import math
# try: 
#     matplotlib.use("module://mplcairo.gtk")
# except:
#     pass

from scipy import fftpack
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
from matplotlib.ticker import ScalarFormatter
import matplotlib.ticker as mticker
import threading
#import jack
import socket
import queue
import collections
#from numba import jit
from calc import Schnittpunkt, MicPos, chirpgenerator,butter_bandpass_filter,convert,ReflexionsPunkt
import openpyscad as ops
from MARV_sudoku import Bildquelle, RaumPunkte
#from numba import jit
import sys
sys.setrecursionlimit(2000)


def Corr_calc(datastorage,channels,fstart=1000,fstop=19000,loadpath="",fs=96000,):#noch kein Channel Stuff # Ausrechnen vom RIR und peakfinding    
    n=0
    used_chirp=5
    # add chirps from loadpath to Analysis/datastorage quque
    for root, dirs, recs in os.walk(loadpath):
        for name in recs:
            if name=="chirp.npy":#recognize chirp Signal data
                chirp_values=np.load(os.path.join(root,name)) 
            try:
                num = int(name[0:-4])
                if num==used_chirp:#recognize target chirp  
                    datastorage.put(np.load(os.path.join(root,name)))
            except ValueError:
                continue

    #load previous recording, or get from queue
    try: 
        data = datastorage.get(timeout=6)
        n+=1
    except queue.Empty:  #wenn nix is mach nix
        return

    chirplen=len(data[0])
    cors=np.zeros((channels,chirplen))
    for c in range(channels):
        rec_chirp = data[c] #select channel
        #rec_chirp = butter_bandpass_filter(rec_chirp, fstart, fstop, fs, order=6)#Filter
        corr_chirp = np.correlate(rec_chirp,chirp_values,"same")#correlate the recording and signal
        corr_chirp = corr_chirp/np.max(corr_chirp)#normalize
        #find first Peak-that corresponds to Signal and use as new 0 Index 
        peaks,peaks_dict=signal.find_peaks(corr_chirp,height=(0.07,None),prominence=(0.07,None),width=2)
        chirp_loc=0 # location of original chirp 
        for index in peaks:
            if corr_chirp[index]*0.3>corr_chirp[chirp_loc]:
                chirp_loc=index
        corr = np.append(corr_chirp[chirp_loc:],np.zeros(chirp_loc))
        #if chirplen-chirp_loc>echo_lens[c]: echo_lens[c]=chirplen-chirp_loc
        cors[c]=corr    
    print("Correlation done")
    for c in range(channels): 
        cors[c]=cors[c]/np.max(cors[c])#normalize
    return cors
    


def peaksorter(cors,channels,fs):
    '''
    cors: correlations
    channels: number of channels
    chirplen:
    echo_lens: länge des echosignals in corr
    '''
    #echo_soll=np.load("_tempechoes.npy")
    chirplen=len(cors[0])
    c_peaks=[1,1,1,1]
    for c in range(channels):
        print(c)
        cors[c]=cors[c]/np.max(cors[c]) #normalization
        
        p=0
        #start_y = 0.1
        change  = 0.2
        minichange=0.01
        #pktgerade = np.linspace(0,(change/75)*chirplen,chirplen) #set up a line for prominence
        d=0.04
        #make e function
        strech = 2
        xs=np.linspace(0,50,chirplen)
        #plt.plot(np.linspace(0,chirplen/192000,chirplen),ys,color="green",label="prominence")
        #plt.show()
        last=int(chirplen/2)
        prominence=0.05
        peak_bol=True
        #Alternate=True
        trys=0
        while peak_bol:
            #apply prominence
        
            #prominence=5**(-xs*strech)+d
            prominence=d
            peaks_fil,_=signal.find_peaks(cors[c][0:chirplen],prominence=prominence,width=2)
            #peaks_fil=np.array([peaks[0]],dtype=int)
            #for i in range(1,len(peaks)-1):
             #   if True: #cors[c][peaks[i-1]]<cors[c][peaks[i]]>cors[c][peaks[i+1]] or (cors[c][peaks[i+1]]-cors[c][peaks[i]] > 500 and cors[c][peaks[i]]- cors[c][peaks[i-1]] >500):
              #      peaks_fil=np.append(peaks_fil,int(peaks[i])) 
                           
            #stop when more than ten and less than 25 are detected
            peak_bol=False
            
            #if not Alternate:
            #peak_bol=True
            if peaks_fil.size > 0:
                if peaks_fil[0] > 600/192000*fs:
                    strech+=change
                    #print("no floor")
                    Alternate=True
                    peak_bol=True

            #elif Alternate:
            if peaks_fil.size>80:
                d+=minichange
                #print("too many")
                peak_bol=True
                Alternate=False
            if peaks_fil.size<12:
                d-=minichange
                #print("too few")
                peak_bol=True
                Alternate=False
            trys+=1
            if trys>100:
                print("could not find peaks")
                peak_bol=False
        # plt.subplot(channels*100+10+c+1)  
        # #plt.plot(np.linspace(0,chirplen/192000,chirplen),prominence,color="green")
        # #plt.plot(np.linspace(0,chirplen/96000,chirplen),prominence,color="green",label="prominence")
        # plt.scatter(peaks_fil/96000, cors[c][peaks_fil], marker="x", s=30, label="Peaks",color="orange")
        # plt.plot(np.linspace(0,chirplen/96000,chirplen),cors[c],label="mic: "+str(c),color="blue")
        # plt.vlines(echo_soll[c],-1,1,colors=["red","green","purple","blue","yellow","pink"])
        # plt.xlabel("Zeit in s")
        # plt.ylabel("Amplitude")
        # plt.legend(loc="upper right") 
        c_peaks[c]=peaks_fil
    #plt.show()
    #print(c_peaks)
    index_min=0
    d_peaks=[np.array([]),np.array([]),np.array([]),np.array([])]
    for i in range(4): 
        if c_peaks[i][-1]<c_peaks[index_min][-1]:
            index_min=i
    for i in range(4):
        for j in range(len(c_peaks[i])):
            if c_peaks[i][j] <= c_peaks[index_min][-1]:
                d_peaks[i]=np.append(d_peaks[i],c_peaks[i][j])
    return d_peaks #return sorted peaks

#@jit(nopython=True)
def Roomcalc(Pos,Dis,echoes):
    RP=RaumPunkte(Pos)
    place_peaks = pd.DataFrame(columns=("x","y","z","p1","p2","p3","p4","error","object"))
    n=0 
    tolerance=0.0#tolerance for intersection of spheres affects runtime massivley
    
    # Dis[i][j] : Distanz der Mikrofone
    # p_i : peak/echo von mic i
    # Iterate all echos from mic 1
    for p1 in echoes[0]: # Go through all combinations of peaks and ignore combos without intersection
        # Iterate all echos from mic 2
        for p2 in echoes[1]:
            #check if intersection is plausible
            if p1+p2<Dis[1][2] -tolerance  or abs(p1-p2) > Dis[1][2]+tolerance:
                continue
            # Iterate all echos from mic 3
            for p3 in echoes[2]:
                if p1+p3<Dis[1][3]-tolerance or p2+p3<Dis[2][3]-tolerance or abs(p1-p3) >Dis[3][1] +tolerance or abs(p2-p3) >Dis[3][2] +tolerance:
                    continue
                # Iterate all echos from mic 4
                for p4 in echoes[3]:
                    if p1+p4<Dis[1][4]-tolerance or p2+p4<Dis[2][4]-tolerance or p3+p4<Dis[3][4]-tolerance or abs(p1-p4) > Dis[1][4]+tolerance or abs(p2-p4) > Dis[4][2]+tolerance or abs(p3-p4) > Dis[3][4] + tolerance:
                        continue
                    try:
                        Point, error = Schnittpunkt(Pos,p1,p2,p3,p4)
                        if Point is None:
                            continue
                    except AttributeError as e:
                        print(e.with_traceback())
                        continue
                    Ref_Point=[]
                    for i in range(1,5):
                        Ref_Point.append(ReflexionsPunkt(Pos[i],BQ = Point))
                    BQ=Bildquelle([p1,p2,p3,p4],error,Point,Ref_Point)
                    Data_A = pd.DataFrame({#for legacy support
                                            "x":Point[0],# remeber is BQ
                                            "y":Point[1],
                                            "z":Point[2],
                                            "p1":p1,
                                            "p2":p2,
                                            "p3":p3,
                                            "p4":p4,
                                            "error":error,
                                            "object":BQ,
                                        },index=[n]) 
                    place_peaks=pd.concat([place_peaks,Data_A])  
                    RP.addBQ(BQ)#all BQs are stored in RP
                    n+=1 
        
    return RP,place_peaks


def main():
    sys.setrecursionlimit(2000)
    #mode  
    plorting=True
    channels = 4
    fs = 98000
    #Parameters
    #Klavierzimmer A1
    Dis=[[0,2.118,2.066,2.30,2.32],#L
     [2.118,0,0.335,0.45,0.36],#Mic0
     [2.066,0.335,0,0.251,0.347],#Mic1
     [2.30,0.45,0.251,0,0.26],#Mic2
     [2.32,0.36,0.347,0.26,0]]#Mic3
     
    datastorage = queue.Queue()# for chirp analysis
    loadpath="./A1/"
    fstart=1000
    fstop=19000

    cors= Corr_calc(datastorage,channels,fstart, fstop, loadpath,fs)
    #np.save(loadpath+"_tempcorr.npy",cors)
    #cors = np.load(loadpath+"_tempcorr.npy")

    #return
    peaks = peaksorter(cors,channels,fs)
    np.save(loadpath+"_tempdpeaks",peaks)
    #peaks   = np.load(loadpath+"_tempdpeaks.npy",allow_pickle=True)
    #echoes = np.load(loadpath+"_tempechoes.npy",allow_pickle=True)
    echoes  = convert(Dis,peaks,fs,channels) #np.cross(a,b)
    Pos,err = MicPos(Dis)
    RP,place_peaks = Roomcalc(Pos,Dis,echoes)
    place_peaks.to_pickle(loadpath+"/place_peak_A1_sudoku.pkl")
    #place_peaks=pd.read_pickle("./pickles/place_peak_D2_new.pkl")
    #pd.set_option("max_rows", 11)
    #print(place_peaks.describe())
    #print(Pos)
    scad=ops.Union()
    speakerIdx = 0
    place_peaks.sort_values(by=["error"],ascending=True)#sort by error to optimize Sudoku
    print("performing Sudoku on:",len(RP.allBQs))
    for BQ in RP.allBQs:
        RP.Sudoku(BQ)    #use the Sudoku funktion on all BQs to decide the best Combination 
    RP.status()
    RP.addPlane([Pos[1]-[0,0,0.05]],Normal=[0,0,1])#add Plane for ground beneat Mics
    RP.Construct()#construct Corners of Room from BQS and added Planes
    print(len(RP.Corners))
    for BQ in RP.used:# write Refpoints in scad file
       for Ref in BQ.Refpoints:
            scad.append(ops.Sphere(r=0.05, _fn=10).translate(list(Ref)).color("Red"))#,((max(error)-(place_peaks["error"][i]))/max(error)))
    for Corner in RP.Corners:# write Corners in scad file  
        scad.append(ops.Sphere(r=0.1, _fn=10).translate(list(Corner)).color("green"))
    # Iterate speaker and microphones
    for i in range(channels+1):
        if i == speakerIdx: # Catch that speaker and paint it orange
            scad.append(ops.Sphere(r=0.05, _fn=10).translate([Pos[i][0], Pos[i][1],Pos[i][2]]).color("Orange"))
        else: # Paint the mics in blue
            scad.append(ops.Sphere(r=0.05, _fn=10).translate([Pos[i][0], Pos[i][1],Pos[i][2]]).color("Blue"))

    scad.write(loadpath+"Auswertung.scad")      
    return


if __name__ == "__main__":
    main()
