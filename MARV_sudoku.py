from ast import List
from ctypes import pointer
import numpy as np 
from itertools import combinations,permutations
import sys
import pandas as pd
sys.setrecursionlimit(2000)
class Bildquelle:
    def __init__(self,Peaks,Error,Bildpunkt,Refpoints) -> None:
        self.Peaks=Peaks#the Peaks used to calc Bildquelle
        self.Prio=Error#hier vielleicht eine Komplexere Funktion benutzen
        self.Bildpunkt=Bildpunkt
        self.Refpoints=Refpoints
        self.blocked=[]
        self.removed=[]
        self.trys=0
    
    def addBlocked(self,Blocked):
        self.blocked.append(Blocked)#tracks BQs that wehere blocked by this BQ
        return

    def setRemoved(self,Removed):
        self.removed=Removed#tracks BQs removed by this BQ
        return

    def getDis(self,Point):
        return np.linalg.norm(self.Refpoints[0]-Point)

class Gerade:
    def __init__(self,Point,Dir) -> None:
        self.Point=np.array(Point)
        self.Dir=np.array(Dir)

    def Point_on(self,t):
        Point_off=self.Point+t*self.Dir
        return Point_off

class Ebene: 
    def __init__(self, Points,id=0,center=[0,0,0],Normal="n") -> None:# initalize Plane using Points or Point and Normal Vektor
        Points=np.array(Points)
        self.FixPoint=Points[0]
        self.center=center
        self.id=id
        if Normal=="n":
            self.Normal=np.cross(Points[0]-Points[1],Points[0]-Points[2])#calculate Normal Vektor
        else:
            self.Normal=Normal
        self.Normal=self.Normal/np.linalg.norm(self.Normal)
        if np.linalg.norm(Points[0]-self.center+self.Normal) > np.linalg.norm(Points[0]-self.center-self.Normal):#does Normal poit outwards
            self.Normal*=-1 # flip dir of Normal Vektor
        self.KorEqual=np.dot(self.Normal,Points[0])# calc the Equals for Koordinate Form
        # for P in Points:
        #     if not self.is_on(P):
        #         raise Exception("Not all Points are one a Plane")

    def is_on(self,Point) -> bool:
        if np.dot((Point-self.FixPoint),self.Normal) == 0 :
            return True
        else:
            return False

    def plane_intersect(self, Ebene) -> Gerade:

        a = [self.Normal[0],self.Normal[1],self.Normal[2],-self.KorEqual]
        b = [Ebene.Normal[0],Ebene.Normal[1],Ebene.Normal[2],-Ebene.KorEqual]
        a_vec, b_vec = np.array(a[:3]), np.array(b[:3])

        aXb_vec = np.cross(a_vec, b_vec)

        A = np.array([a_vec, b_vec, aXb_vec])
        d = np.array([-a[3], -b[3], 0.]).reshape(3,1)

        # could add  test to prevent linalg.solve throwing error
        phi=np.degrees(np.arccos(np.dot(self.Normal,Ebene.Normal)/np.linalg.norm(self.Normal)*np.linalg.norm(Ebene.Normal)))
        #check if planes are nearly parallel
        if 170 < phi or phi < 10:
            raise ArithmeticError("Plains dont intersect")
        p_inter = np.linalg.solve(A, d).T
        sect=Gerade(p_inter[0],(p_inter + aXb_vec)[0]-p_inter[0])
        return sect

    def intersect_Gerade(self,line:Gerade) -> list:
        phi=np.degrees(np.arccos(np.dot(self.Normal,line.Dir)/np.linalg.norm(self.Normal)*np.linalg.norm(line.Dir)))
        if 80<phi and 10>phi:
            raise ArithmeticError("line and plain dont intersect")
        t=(self.KorEqual-self.Normal[0]*line.Point[0]-self.Normal[1]*line.Point[1]-self.Normal[2]*line.Point[2])/(self.Normal[0]*line.Dir[0]+self.Normal[1]*line.Dir[1]+self.Normal[2]*line.Dir[2])
        Point=line.Point_on(t)
        return t,Point

    def is_behind(self,Point) -> bool:#check if a Point is behind Plane
        Lot_Gerade=Gerade(Point,self.Normal)
        t,Lot_Point=self.intersect_Gerade(Lot_Gerade)
        Behind=True
        if t<=0.1:
            Behind = False
        return Behind         


    
class RaumPunkte:
    def __init__(self,Pos) -> None:
        self.used=[]#used BQs
        self.usedPeaks=[[],[],[],[]]#Peaks of used BQs by channel
        self.allBQs=[]
        self.nSudoku=0
        self.planes=[]
        self.Corners=[]
        self.Pos=Pos
        self.center=[0,0,0]
        for i in range(4):
            self.center+=Pos[i+1]
        self.center/=4
        self.order=[]

    def addUsed(self,Bildquelle:Bildquelle):#add new BQ to Used BQs and Peaks
        self.used.append(Bildquelle)
        for i in range(4):
            self.usedPeaks[i].append(Bildquelle.Peaks[i])
    
    def addBQ(self,BQ:Bildquelle):
        self.allBQs.append(BQ)
        
    def _isused(self,Peaks):# check if one or more of Peaks is used
        clashBQs=[]
        clash=False
        for i in range(4):
            if Peaks[i] in self.usedPeaks[i]:
                clashBQs.append(self.used[self.usedPeaks[i].index(Peaks[i])])
                clash=True
        clashBQs=list(set(clashBQs))
        return clash,clashBQs
    
    def getPrioList(self,Bildquellen):#suche die Priorität einer Liste von BQs 
        Prio=0
        for BQ in Bildquellen:
            if BQ.Prio > Prio:# im Moment noch das Maximum muss man Testen ob was anderes bessere Ergebnisse Liefert
                Prio=BQ.Prio
        return Prio

    def Sudoku(self,Bildquelle:Bildquelle):
        #ordererd by error gives better results
        self.nSudoku+=1
        Bildquelle.trys+=1
        clash,clashBQ=self._isused(Bildquelle.Peaks)#check if used
        if clash:
            Prio=self.getPrioList(clashBQ)#vielleicht sollte man hier denn durschnitt dder Konflikte nehmen? nocht sicher
            if Bildquelle.Prio < Prio:
                Bildquelle.setRemoved(clashBQ)#track removed BQs
                self.addUsed(Bildquelle)#add BQ to used
                for Conflict in clashBQ:    
                    self.used.remove(Conflict)#remove BQs from used   
                    for i in range(4):
                        self.usedPeaks[i].remove(Conflict.Peaks[i])# removed Peaks
                for Conflict in clashBQ:      
                    for retry in Conflict.removed+Conflict.blocked:#check if one of the blocked or removed BQs is now valid
                        try:
                            self.Sudoku(retry)
                        except RecursionError:
                            print(f"Recursion error on iteration {self.nSudoku}")
                            pass
        else:
            self.addUsed(Bildquelle)
    

    def status(self):
        print("RP status:")
        print("Sudoku Durchläufe:",self.nSudoku)
        print("selected",len(self.used),"BQs")
        print("using the following trys")
        print([BQ.trys for BQ in self.used])
        print("average Prio:",sum([BQ.Prio for BQ in self.used])/len(self.used))

    def calc_center(self):#as center of all Bildpunkte 
        Point=[0,0,0]
        for BQ in self.used:
            Point+=BQ.Bildpunkt
        self.center=Point/len(self.used)

    def addPlane(self,Points,id=0,Normal="n"):
        #try:
        newPlane=Ebene(Points,id,self.center,Normal=Normal) #check if plane is valid
        #except Exception:
       #     print("not all on a Plane")
        #    return
        if len(self.planes)==0:#is it the first plane
            self.planes.append(newPlane)
            return
        for Pl in self.planes:#check if Point is behind an other Plane because Walls cnt be behind Walls
            if Pl.is_behind(Points[0]):
                 return
        self.planes.append(newPlane)

    def Construct(self):
        i=0
        Dist=[BQ.getDis(self.center) for BQ in self.used]
        Order=pd.DataFrame({"dist":Dist})
        Order.sort_values(by=["dist"],ascending=True)#order BQ by dist to center
        if not self.used == []:# if planes are added manualy 
            self.calc_center()
        for i in Order.index:# add planes from BQs
            BQ = self.used[i]
            #RefPoint=BQ.Refpoints
            RefPoint=np.mean(BQ.Refpoints,axis=0)#calculate mean of Refpoints to get point on Wall 
            Norm=BQ.Bildpunkt-self.Pos[0]#Norm vektor from Bildpunkt to Speaker
            if np.linalg.norm(RefPoint-self.center) < 1:
                continue
            self.addPlane(RefPoint,Normal=Norm)

        for Planes in combinations(self.planes,3):#every combnation of 3 planes to get every Corner
            try:
                Section = Planes[0].plane_intersect(Planes[1]) #
            except ArithmeticError:
                continue
            try:    
                t,Point = Planes[2].intersect_Gerade(Section)
            except ArithmeticError:
                continue
            self.Corners.append(Point)
        self.Corners=np.unique(np.around(self.Corners,decimals=4),axis=0)#round to 4rd decimal and remove duplicates
