# A1 6.1.22
Raummaße: 4.977 x 5.12 x 2.765
Neue Aufnahmen Klavierzimmer
useful chirps: 3-25
chirp 1000 -> 19000Hz dur 0.5 single hyp
Wand abstände
L x=1.81
L y=0.77

m0 Ecke neuer 0 punkt


m0 x=2.35
m0 y=2.66

Dis=[[0,2.118,2.066,2.30,2.32],#L
     [2.118,0,0.335,0.45,0.36],#Mic0
     [2.066,0.335,0,0.251,0.347],#Mic1
     [2.30,0.45,0.251,0,0.26],#Mic2
     [2.32,0.36,0.347,0.26,0]]#Mic3