from numpy.linalg import norm
import numpy as np
from scipy.optimize import fsolve, minimize
from mpl_toolkits.mplot3d import axes3d
import matplotlib.pyplot as plt
from scipy.signal import chirp 
from numpy import asarray as ar,exp
from scipy import signal
from scipy import constants

#Set Speaker as Origin
L = np.array([0.0,0.0,0.0])
# "Gleichungssystem"
def MicPos(Dis):
    def f(args):
        m11,m12,_,m21,m22,_,m31,m32,_,_=args
        opt=np.zeros(10)
        n=0
        Pos=np.array([L,[0.54,1.89,-1],[m11,m12,-0.795],[m21,m22,-0.79],[m31,m32,-0.76]])
        for i in range(5):
            for j in range(i+1,5):
                opt[n]=norm(Pos[i]-Pos[j])-Dis[i][j]
                n+=1
        return opt
    # Solve Mics
    #guess=[Dis[0][2],Dis[1][2],1,Dis[0][3],Dis[1][3],1,Dis[0][4],Dis[1][4],1,0,0,0,0,0,0]
    guess=np.zeros(10)+1
    sol=fsolve(f,guess)
    #m02,m11,m12,m21,m22,m31,m32,_,_,_=sol
    m11,m12,_,m21,m22,_,m31,m32,_,_=sol
    Pos=np.array([L,[0.54,1.89,-1],[m11,m12,-0.795],[m21,m22,-0.79],[m31,m32,-0.76]])
    #Pos=np.array([L,[1.80,m02,-0.24],[m11,m12,-0.24],[m21,m22,-0.24],[m31,m32,-0.24]])
    return Pos, f(sol)

def Schnittpunkt(Pos,R1,R2,R3,R4):

    def Kugel(a,r,u,v):
        """Funktions Gleichung einer Kugel
        """
        return a + r*np.array([np.cos(u)*np.sin(v),np.sin(u)*np.sin(v),np.cos(v)])

    def f(args):
        #x,y,z=args
        res=0
        res+=np.power(norm(Pos[1]-args)-R1,2) 
        res+=np.power(norm(Pos[2]-args)-R2,2) 
        res+=np.power(norm(Pos[3]-args)-R3,2) 
        res+=np.power(norm(Pos[4]-args)-R4,2) 
    

        return res
    bestcoord = [0,0,0]
    bestvalue = 10000000
    n_montecarlo=100 # no use using high numbers
    range_montecarlo=5 # 5 is a good number
    #start = [10*np.random.rand(3)-5 for i in range(n_montecarlo)]
    for i in range(n_montecarlo): #40
        coord = 2*range_montecarlo*np.random.rand(3)-range_montecarlo
        error=f(coord)
        if( error < bestvalue):
            bestcoord = coord
            bestvalue = error

    #print(np.where( f(start) == min(f(start))))
    #minimize_res = minimize(f,start[np.where( f(start) == min(f(start)))[0]]) #   [0,0,0])
    #minimize_res = minimize(f,[0,0,0]) #   [0,0,0])
    minimize_res = minimize(f, bestcoord) #   [0,0,0])
    sol = minimize_res.x

    error=f(sol)
    
    return sol,error

def ReflexionsPunkt(Mic,BQ):
    Mic=np.array(Mic)
    BQ=np.array(BQ)
    def gerade(r):
        return Mic + r*(BQ - Mic)
    def ralc(r):
        R=gerade(r)
        return norm(norm(R-Mic)+norm(R)-norm(Mic-BQ))
    guess=0.5
    sol=minimize(ralc,guess,bounds=[[0,1]])
    return gerade(sol.x)
 


# def Schnittpunkt(Pos,R1,R2,R3,R4):
#     def Kugel(a,r,u,v):# Funktions Gleichung einer Kugel  
#         return a + r*np.array([np.cos(u)*np.sin(v),np.sin(u)*np.sin(v),np.cos(v)])
#     def f(args):
#         u1,v1,u2,v2,u3,v3,u4,v4,_,_ = args #Eingabe
#         Points=np.zeros((4,3))
#         Points[0] = Kugel(Pos[1],R1,u1,v1)
#         Points[1] = Kugel(Pos[2],R2,u2,v2)
#         Points[2] = Kugel(Pos[3],R3,u3,v3)
#         Points[3] = Kugel(Pos[4],R4,u4,v4)
#         #opt=np.zeros(10)
#         n=0
#         res=0
#         #for i in range(4):# Fehler eines Punktes
#         vec_ges = [0,0,0]
#         for i in range(4):
#             vec_ges += Points[i] * (-1)**i
#             #opt[n]+=norm(Points[i]-Points[j])
#         res=norm(vec_ges)
#         n+=1    
#         return res
#     # solve f 
#     startval=0.2
#     bounds=[(0,2*np.pi),(0,np.pi),(0,2*np.pi),(0,np.pi),(0,2*np.pi),(0,np.pi),(0,2*np.pi),(0,np.pi),(0.01,0.1),(0.01,0.1)]
#     guess=np.array([startval,startval,startval,startval,startval,startval,startval,startval,0,0])
#     #guess=np.array([np.pi,np.pi,np.pi,np.pi,np.pi,np.pi,np.pi,np.pi,0,0])
#     minimize_res= minimize(f,guess,bounds=bounds)
#     sol = minimize_res.x
#     '''
#     for i in sol[:-2]:
#         if i == startval:
#             return None, None
#     '''
#     Point=Kugel(Pos[1],R1,sol[0],sol[1])+Kugel(Pos[2],R2,sol[2],sol[3])+Kugel(Pos[3],R3,sol[4],sol[5])+Kugel(Pos[4],R4,sol[6],sol[7])
#     Point=Point/4
#     error=f(sol)
#     return Point,error


def chirpgenerator(play,blocksize,fstart = 1000, fstop=19000,chirp_dur=0.3,fs = 96000,channels=1):    
    #samples?
    samples=int(fs*chirp_dur)
    overhead=samples % blocksize 
    samples = samples-overhead
    #t u. buffer
    chirp_dur=samples/fs
    t=np.linspace(0,chirp_dur,samples)
    buffer=np.zeros(2*samples)

    #chirp
    chirp_values=chirp(t,fstart,chirp_dur,fstop,method="hyp") # 10000 only for laptop recordings bc bad microphones
    values=np.append(chirp_values,buffer)
    values=np.append(buffer,values)
    # schreiben in die queue
    chunked=values.reshape(int(len(values)/blocksize),blocksize)
    return (chirp_values, samples, chunked)

def butter_bandpass(lowcut, highcut, fs, order=5):
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq
    b, a = signal.butter(order, [low, high], btype='band')
    return b, a

def convert(Dis,peaks,fs,channels):
    #channels=peaks.shape[0]
    echoes= [[]]*channels
    for c in range(channels):
        peakdistance = peaks[c]/fs*constants.mach
        echoes[c] = Dis[0][c+1]+ peakdistance
    return np.array(echoes)

def butter_bandpass_filter(data, lowcut, highcut, fs, order=5):
    b, a = butter_bandpass(lowcut, highcut, fs, order=order)
    filtered_data = signal.lfilter(b, a, data)
    return filtered_data