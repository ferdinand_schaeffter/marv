import numpy as np
from cv2 import imread
from skimage.color import rgb2gray


a = imread('img235.jpg');
a = rgb2gray(a);
msk = a > 30;
props = regionprops(logical(msk), 'Area');
allAreas = sort([props.Area]);
msk = bwareaopen(msk, 25);
msk = imclearborder(msk);
bw2 = bwareafilt(msk,1);
BW2 = imfill(bw2,'holes');
