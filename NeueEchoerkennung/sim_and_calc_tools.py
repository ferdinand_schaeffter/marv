import numpy as np
from random import random
import matplotlib.pyplot as plt

def make_signal(len_sig, A=1, omega=1, phase_shift=0, noise=False, dev=1, mean=0):
    if phase_shift == "rand": phase_shift = random()*np.pi*2
    signal = np.array([A * np.sin(omega * (x + phase_shift)/np.pi) for x in range(len_sig)])
    if noise: signal += np.random.normal(mean, dev, len_sig)
    return signal



def get_peaks(signal):
    '''Very makeshift! Quick n dirty solution
    -> isnt there a find_peaks already?
    -> look for chirp width instead of current solution
    '''
    signal = np.abs(signal)

    maximum = max(signal)
    threshold = maximum*0.9

    peaks = []
    i = 0
    while i < len(signal):
        if signal[i] >= threshold:
            item = [[signal[i]],[i]]
            while signal[i] >= threshold and i < len(signal)-1:
                i+=1
                item[0].append(signal[i]); item[1].append(i)

            peaks.append(item[1][np.argmax(item[0])] / 100) # Was wenn nicht eindeutig??? # Und warum mal hundert??? siehe comparison.py
        i += 1
    return peaks