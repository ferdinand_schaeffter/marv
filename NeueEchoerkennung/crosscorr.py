import statsmodels.api as sm
import numpy as np


def cross_correlation(signal, chirp):
    len_chirp = len(chirp)
    len_corr_sig = len(signal)-len_chirp
    corr_signal = np.zeros(len_corr_sig)

    for i in range(len_corr_sig):
        corr_signal[i] = np.corrcoef(signal[i:i+len_chirp], chirp)[0,1]

    return corr_signal
