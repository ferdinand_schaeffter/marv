import wave
import struct
import numpy as np
from scipy.signal import chirp 
import os
import wave
import time
from scipy.io import wavfile
from scipy import signal
from scipy import constants
import numpy as np
import matplotlib as mpl
import matplotlib
matplotlib.use("module://mplcairo.gtk")

import matplotlib.pyplot as plt

fsignal = 50.1
tstart = 0.0
tstop = 100.0
wavelower = 1.0
waveupper = 1000.0

'''
widths = np.linspace(wavelower, waveupper, 500)
cwtmatr = signal.cwt(testsignal, signal.ricker, widths)
plt.imshow(cwtmatr, extent=[-1, 1, widths, cwtmatr], aspect='auto')#,vmax=abs(cwtmatr).max(), vmin=-abs(cwtmatr).max())
'''

t = np.linspace(0, 20, 200000, endpoint=False)
sig  = np.cos(2 * np.pi * (fsignal +t) * t)
widths = np.linspace(wavelower, waveupper, 500)
cwtmatr = signal.cwt(sig, signal.ricker, widths)
plt.imshow(cwtmatr, cmap='PRGn', aspect='auto',
           vmax=abs(cwtmatr).max(), vmin=-abs(cwtmatr).max())
plt.show()