import statsmodels.api as sm
import numpy as np

def autocorrelation(signal, i=1):
    ''' Here signal will be autocorrelated i times.
        input:
            signal:
                np.array([domain, noisy signal])
            i:
                int (or float)
        output:
            np.array([domain, signal autocorrelated i times])

        Always autocorrelates at least 1 time, even with i == 0 or i < 0.
    '''
    
    # Break if signal is just zeros
    empty = True
    for element in signal:
        if element != 0:
            empty = False
            break
    if empty: 
        return signal

    h = 1

    def autocorrelate(signal, h):
        autocorrelated_signal = sm.tsa.acf(signal, fft=False, nlags=len(signal))
        if h<i:
            h+=1
            return autocorrelate(autocorrelated_signal, h)
        return autocorrelated_signal, h

    autocorrelated_signal, h  = autocorrelate(signal, h)

    return np.array(autocorrelated_signal[0:])
