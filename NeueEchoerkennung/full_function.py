import numpy as np
import matplotlib.pyplot as plt
import sys

from sim_and_calc_tools import make_signal, get_peaks
from hff import get_hff
from autocorr import autocorrelation
from crosscorr import cross_correlation


autocorr_iters = 2
percent_of_chirp = 0.01
smoothing_strength = 1

def analysis(data):
    data = autocorrelation(data, i=autocorr_iters)
    _, freq = get_hff(signal, delta=smoothing_strength)
    return freq

def structurer(signal, chirp):
    freqs, freqs_chirp=[],[]

    for position in range(len(chirp)):
        data = chirp[position:position+round(len(chirp)*percent_of_chirp)]
        freqs_chirp.append(analysis(data))


    for position in range(len(signal)-len(chirp)):
        data = signal[position:position+round(len(chirp)*percent_of_chirp)]
        freqs.append(analysis(data))

    chirp_positions = cross_correlation(freqs, freqs_chirp)

    return chirp_positions


def main_(signal, chirp):
    chirppos = structurer(signal, chirp)
    plt.plot(np.arange(len(chirppos)), chirppos)
    plt.show()
