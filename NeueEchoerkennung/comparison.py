import numpy as np
import matplotlib.pyplot as plt
import sys

from sim_and_calc_tools import make_signal, get_peaks
from hff import get_hff
from autocorr import autocorrelation
from crosscorr import cross_correlation


'''
- make_sample_array
- get_freqs
- plot_func
'''

def proc_deviation(error, abs):
    return (abs-error)/abs

def make_sample_array(len_sig):
    '''
    Input: int
        signal len
    Output: np_array, np.array
        matrix of signals of form: [ omega1:[dev1, dev2, dev3], omega2:[dev1, dev2, dev3], omega3:[dev1, dev2, dev3]]
        matrix with indexes; elements are in form: (frequency, deviation)
    '''
    sample_array, index_array = [], []
    for omega in range(0, 20, 1):
        omega /= 10
        sample_row, index_row = [],[]
        for sigma in range(0, 20, 1):
            sigma /= 10
            sample_row.append(make_signal(len_sig, omega=omega, phase_shift="rand", noise=True, dev=sigma))
            index_row.append((omega/10, sigma)) #WARUM /10?? IST SIGMA AUCH /10 =??  --------------------------- ATTENTION
        sample_array.append(sample_row)
        index_array.append(index_row)

    return np.array(sample_array), np.array(index_array)


def get_result_matrix(array, indexes, method):
    truth_matrix = np.zeros(array.shape[:2])

    if method[:3] == "hff":

        if len(method) > 3:
            i = int(method[3:])
        else: i=0

        for row_num in range(len(array)):
            for signal_num in range(len(array[row_num])):
                signal = array[row_num][signal_num]
                if i >= 1: signal = autocorrelation(signal, i=i)

                if indexes[row_num,signal_num][0] == 0 and indexes[row_num,signal_num][1] == 0:
                    freq = 0
                else:
                    _, freq = get_hff(signal)

                if freq == indexes[row_num,signal_num][0]: truth_matrix[row_num, signal_num] = 1

    elif method[:5] == "plain":

        if len(method) > 5:
            i = int(method[5:])
        else: i=0

        for row_num in range(len(array)):
            for signal_num in range(len(array[row_num])):
                signal = array[row_num][signal_num]
                if i >=1: signal = autocorrelation(signal, i=i)
                freq = max(get_peaks(np.fft.rfft(signal)))
                if freq == indexes[row_num,signal_num][0]: truth_matrix[row_num, signal_num] = 1
                print(freq, indexes[row_num,signal_num][0])

    print(truth_matrix)
    return truth_matrix


def plot_func(func):
    sample_array, index_array = make_sample_array(200)
    for name in ["plain", "plain1", "plain2", "plain3", "hff", "hff1", "hff2", "hff3"]:
        plt.matshow(get_result_matrix(sample_array, index_array, name))
        plt.title(name)
        plt.show()


if __name__ == "__main__":
    print("start")
    for func in sys.stdin:
        plot_func(func.rstrip())
